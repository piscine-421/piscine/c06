/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/25 21:23:55 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	main(int argc, char **argv)
{
	int	argnum;
	int	count;
	int	index;
	int	smallest;

	count = argc - 1;
	while (count > 0 && count--)
	{
		argnum = 1;
		smallest = 1;
		while (argnum != (argc - 1) && argnum++)
		{
			index = 0;
			while (argv[argnum][index] == argv[smallest][index])
				index++;
			if (argv[argnum][index] < argv[smallest][index])
				smallest = argnum;
		}
		index = 0;
		while (argv[smallest][index])
			write(1, &argv[smallest][index++], 1);
		argv[smallest][0] = 127;
		write(1, "\n", 1);
	}
}
